# Description

Internal entities allows you to modify any existing entity bundle and declare it as 'internal'.

This allows you to return a 404 (Not Found), 403 (Access Denied) or a 301 (Redirect) when the view route of this entity is requested.

https://www.drupal.org/node/212219://www.drupal.org/node/2122195
